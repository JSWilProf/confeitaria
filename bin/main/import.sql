SET IDENTITY_INSERT curso_web.dbo.Confeito ON;
INSERT INTO curso_web.dbo.Confeito (idConfeito,nome,preco) VALUES (1,'Bolo de Trigo', 25.40),(2,'Bolo de Maçã', 27.50),(3,'Bolo de Banana', 22.90);
SET IDENTITY_INSERT curso_web.dbo.Confeito OFF;

SET IDENTITY_INSERT curso_web.dbo.Ingrediente ON;
INSERT INTO curso_web.dbo.Ingrediente (idIngrediente,nome,preco) VALUES (1,'Leite C', 2.90),(2,'Frutose', 0.50);
SET IDENTITY_INSERT curso_web.dbo.Ingrediente OFF;

INSERT INTO curso_web.dbo.Acucar (idAcucar,peso) VALUES (2, 3);

INSERT INTO curso_web.dbo.Leite (idLeite,litro) VALUES (1, 0.5);

INSERT INTO curso_web.dbo.IngredientesParaConfeitos (idConfeito,idIngrediente) VALUES (1,1),(1,2),(2,1),(2,2),(3,1),(3,2);


IF OBJECT_ID('curso_web.dbo.users', 'U') IS NOT NULL;DROP TABLE curso_web.dbo.users;go
IF OBJECT_ID('curso_web.dbo.authorities', 'U') IS NOT NULL;DROP TABLE curso_web.dbo.authorities;go 

CREATE TABLE curso_web.dbo.users ( username nvarchar(20) NOT NULL, password nvarchar(255) NOT NULL, enabled INT, PRIMARY KEY (username) );
CREATE TABLE curso_web.dbo.authorities ( username nvarchar(20) NOT NULL, authority nvarchar(20) NOT NULL, PRIMARY KEY (username) );

INSERT INTO curso_web.dbo.users (username,password,enabled) VALUES ('admin','$2a$10$exqBdW51q/C8CkH2sb/cGeiXMQWsYS7vdRmuq3EPGGbEJNxy/RFWi',1),('usuario','$2a$10$bTL6SMXLkE4PoiL0qee1SOw2esA6CfMjcYMq5XSziQm/jvYG1ARKC',1);
INSERT INTO curso_web.dbo.authorities (username, authority) VALUES ('admin','ROLE_ADMIN'),('usuario','ROLE_USER');

