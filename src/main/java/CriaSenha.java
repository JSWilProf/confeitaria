import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CriaSenha {
	public static void main(String[] args) {
		String senha = JOptionPane.showInputDialog("Informe a senha");
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		FileDialog tela = new FileDialog((Frame)null, "Selecione o Arquivo", FileDialog.SAVE);
		tela.setVisible(true);
		
		try (PrintWriter out = new PrintWriter(tela.getDirectory() + tela.getFile()) ) {
			String encodedPassword = encoder.encode(senha);
			System.out.println(encodedPassword);
			out.println(encodedPassword);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try (BufferedReader in = new BufferedReader(new FileReader(tela.getDirectory() + tela.getFile())) ) {
			String encodedPassword = in.readLine();
			System.out.println(encodedPassword);
			System.out.println("Teste: " + encoder.matches(senha, encodedPassword));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}