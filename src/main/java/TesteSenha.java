import javax.swing.JOptionPane;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TesteSenha {
	public static void main(String[] args) {
		String senha = JOptionPane.showInputDialog("Informe o hash da senha");
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		final String teste = "admin";
		while(!senha.equals("fim")) {
			System.out.println("Teste: " + encoder.matches(teste, senha));
			
			senha = JOptionPane.showInputDialog("Informe a senha");
		}
	}
}
