

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

//@Configuration
public class AppConfig {
//	@Autowired
	private Environment environment;
	
//	@Bean(name="DataSource")
	public DriverManagerDataSource dataSource() {
		String driver = environment.getProperty("spring.datasource.driver-class-name");
		String url = environment.getProperty("spring.datasource.url");
		String user = environment.getProperty("spring.datasource.username");
		String senha = environment.getProperty("spring.datasource.password");
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(driver);
		dataSource.setUrl(url);
		dataSource.setUsername(user);
		dataSource.setPassword(senha);
		
		return dataSource;
	}
}
