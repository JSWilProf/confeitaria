package br.senai.sp.informatica.confeitaria.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.Data;

@Data
@Entity
public class Confeito {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idConfeito;
	private String nome;
	private double preco;
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="IngredientesParaConfeitos",
			   joinColumns={@JoinColumn(name="idConfeito")},
			   inverseJoinColumns={@JoinColumn(name="idIngrediente")})
	private List<Ingrediente> ingredientes;
}
