package br.senai.sp.informatica.confeitaria.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import br.senai.sp.informatica.confeitaria.model.Ingrediente;

public interface IngredienteRepository extends JpaRepository<Ingrediente, Integer> {
}
