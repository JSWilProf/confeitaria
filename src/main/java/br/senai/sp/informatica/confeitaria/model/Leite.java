package br.senai.sp.informatica.confeitaria.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import lombok.Getter;
import lombok.Setter;

@Entity
@PrimaryKeyJoinColumn(name="idLeite")
public class Leite extends Ingrediente {
	@Getter
	@Setter 
	private double litro;
}
