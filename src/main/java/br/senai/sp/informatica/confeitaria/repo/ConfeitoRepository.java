package br.senai.sp.informatica.confeitaria.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.senai.sp.informatica.confeitaria.model.Confeito;

@RepositoryRestResource(path="confeitos")
public interface ConfeitoRepository extends JpaRepository<Confeito, Integer> {
}
