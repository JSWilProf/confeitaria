package br.senai.sp.informatica.confeitaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfeitariaBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfeitariaBootApplication.class, args);
	}
}
