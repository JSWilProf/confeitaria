package br.senai.sp.informatica.confeitaria.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.Data;

@Data
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "tipo")
@JsonSubTypes({@Type(value = Acucar.class, name = "A"), @Type(value = Leite.class, name = "L")})
public abstract class Ingrediente {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idIngrediente;
	private String nome;
	private double preco;
}
