FROM gradle:6.6.1-jdk11-hotspot as builder
WORKDIR /app
RUN git clone https://gitlab.com/JSWilProf/confeitaria.git && \
    cd confeitaria && \
    ./gradlew bootJar
    
FROM adoptopenjdk:11-jdk-hotspot
VOLUME /tmp
ENV HOSTNAME localhost \
    PORT 1433 \
    SERVERPORT 80
COPY --from=builder /app/confeitaria/build/libs/ConfeitariaBoot-0.0.2.jar /tmp/app.jar
EXPOSE 80
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/tmp/app.jar","--hostname=${HOSTNAME}","--port=${PORT}","--serverport=${SERVERPORT}"]