# Criar a rede para o acesso ao mysql
# docker network create -d bridge internal_net

# Cria o container do Database
# docker run -d -p 1433:1433 \
# 	--name database_loja \
# 	--network="internal_net" \
# 	-e ACCEPT_EULA=Y \
# 	-e MSSQL_SA_PASSWORD=vUTjqTu7R_wL8EYX \
# 	-v /opt/data/mssql:/var/opt/mssql \
# 	mcr.microsoft.com/mssql/server:2017-latest

# Criar o docker Image do backend
git push origin master
docker build --no-cache -t curso_web .

# Criar o conteiner do backend
docker rm curso_web
docker run -d -p 80:80 \
	--name curso_web \
	--network="internal_net" \
	-e HOSTNAME=database_loja \
	-e PORT=1433 \
	-e SERVERPORT=80 \
	curso_web

docker logs curso_web -f